require 'spec_helper'
require 'pp'

TERMINAL = '1511467432918DEMO'
TERMINAL_SECRET = 'qo8abll7mmc3857s'

describe TinkoffAq do
  let(:check_params) {
    {
      requestDatetime: '2011-05-04T20:38:00.000+04:00',
      action: 'checkOrder',
      md5: '1B35ABE38AA54F2931B0C58646FD1321',
      shopId: 13,
      shopArticelId: 456,
      invoiceId: 55,
      customerNumber: 812_329_446_9,
      orderCreatedDatetime: '2011-05-04T20:38:00.000+04:00',
      orderSumAmount: 87.10,
      orderSumCurrencyPaycash: 643,
      orderSumBankPaycash: 1001,
      shopSumAmount: 86.23,
      shopSumCurrencyPaycash: 643,
      shopSumBankPaycash: 101,
      paymentPayerCode: 420_071_483_20,
      paymentType: 'AC'
    }
  }

  let(:pay_params) {
    {
      requestDatetime: '2011-05-04T20:38:00.000+04:00',
      action: 'paymentAviso',
      md5: '79512CBC0AE0112D029E9CCFA4BBDA88',
      shopId: 13,
      shopArticelId: 456,
      invoiceId: 55,
      customerNumber: 812_329_446_9,
      orderCreatedDatetime: '2011-05-04T20:38:00.000+04:00',
      orderSumAmount: 87.10,
      orderSumCurrencyPaycash: 643,
      orderSumBankPaycash: 1001,
      shopSumAmount: 86.23,
      shopSumCurrencyPaycash: 643,
      shopSumBankPaycash: 101,
      paymentPayerCode: 420_071_483_20,
      paymentType: 'AC'
    }
  }

  it 'should create terminal' do
    t = TinkoffAq::Terminal.new(TERMINAL, TERMINAL_SECRET)
    expect(t.terminal_key).to eq(TERMINAL)
  end

  it 'should init test order' do
    t = TinkoffAq::Terminal.new(TERMINAL, TERMINAL_SECRET)
    pp t.init_payment('1112', 4000)
  end

  it 'should add customer' do
    t = TinkoffAq::Terminal.new(TERMINAL, TERMINAL_SECRET)
    pp t.add_custumer(7687364827364827346)
  end

end
