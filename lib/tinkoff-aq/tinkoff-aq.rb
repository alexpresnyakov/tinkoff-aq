require 'digest'
require 'http'

module TinkoffAq

  class Logger
    def <<(it)
      pp it
    end
  end

  class Api
    BASE_URL = 'https://securepay.tinkoff.ru/v2'

    def initialize(terminal)
      @term = terminal
      @log = Logger.new
    end

    def post(name, params)
      url = [BASE_URL, '/', name].join
      @log << url
      token = TinkoffAq::sign(params, @term.secret)
      params[:Token] = token
      @log << params
      res = HTTP.post(url, json: params)
      @log << res.parse
      res.parse
    end
  end

  class Terminal
    STATUSES = ['AUTHORIZED',
                'CONFIRMED',
                'REVERSED',
                'REFUNDED',
                'PARTIAL_REFUNDED',
                'REJECTED']

    attr_accessor :terminal_key, :secret

    def initialize(terminal_key, secret)
      @terminal_key = terminal_key
      @secret = secret

      @api = Api.new(self)
    end

    def init_payment(order_id, amount, lang = 'ru', receipt = nil, urls = nil)
      h = {
        TerminalKey: @terminal_key,
        Amount: amount,
        OrderId: order_id,
        Language: lang
      }

      if urls.present?
        h[:SuccessURL] = urls[:SuccessURL]
        h[:FailURL] = urls[:FailURL]
      end

      if receipt.present?
        h[:Receipt] = receipt
      end
      @api.post('Init', h)
    end

    def init_recurrent_payment(order_id, customer_id, amount, lang = 'ru', receipt = nil, urls = nil)
      h = {
        TerminalKey: @terminal_key,
        Amount: amount,
        OrderId: order_id,
        Language: lang,
        CustomerKey: customer_id,
        Recurrent: 'Y'
      }

      if urls.present?
        h[:SuccessURL] = urls[:SuccessURL]
        h[:FailURL] = urls[:FailURL]
      end
      
      if receipt.present?
        h[:Receipt] = receipt
      end

      @api.post('Init', h)
    end

    def charge_summ(payment_id, rebill_id)
      @api.post('Charge', {
        TerminalKey: @terminal_key,
        PaymentId: payment_id,
        RebillId: rebill_id
      })
    end

    def add_custumer(custumer_id)
      @api.post('AddCustomer', {
          TerminalKey: @terminal_key,
          CustomerKey: custumer_id
        }
      )
    end

    def remove_custumer(custumer_id)
      @api.post('GetCustomer', {
          TerminalKey: @terminal_key,
          CustomerKey: custumer_id
        }
      )
    end
    
    def get_state(payment_id)
      @api.post('GetState', {
        TerminalKey: @terminal_key,
        PaymentId: payment_id
      })
    end

    def get_order_state(order_id)
      @api.post('CheckOrder', {
        TerminalKey: @terminal_key,
        OrderId: order_id
      })
    end

    def get_card_list(custumer_id)
      @api.post('GetCardList', {
        TerminalKey: @terminal_key,
        CustomerKey: custumer_id
      })
    end

    def notification(params)

    end

    def cancel(payment_id, amount = nil)
      h = {
        TerminalKey: @terminal_key,
        PaymentId: payment_id
      }
      if amount.present?
        h[:Amount] = amount
      end
      @api.post('Cancel', h)
    end

    def confirm(payment_id, amount = nil)
      h = {
        TerminalKey: @terminal_key,
        PaymentId: payment_id
      }
      if amount.present?
        h[:Amount] = amount
      end
      @api.post('Confirm', h)
    end
  end

  def self.sign(params, secret)
    h = {}
    params.each do |k, v|
      h[k] = v
    end

    h[:Password] = secret
    to_sign = h.keys.sort.map { |it| h[it] }.join

    Digest::SHA256.hexdigest to_sign
  end
end
