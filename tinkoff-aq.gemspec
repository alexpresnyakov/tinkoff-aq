
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tinkoff-aq/version'

Gem::Specification.new do |spec|
  spec.name             = 'tinkoff-aq'
  spec.version          = TinkoffAq::VERSION
  spec.authors       = ["Alexander Presnyakov"]
  spec.email         = ["alex@pixella.ru"]
  spec.summary       = %q{Gem for tinkoff aquiring}
  spec.description   = %q{}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_runtime_dependency "activesupport"
  spec.add_runtime_dependency "http"
  spec.add_runtime_dependency "nokogiri"
end
